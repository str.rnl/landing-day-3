import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import User from ".";
import ViewUser from "./view";
import { UserContextProvider } from "./userContext";
import { AboutPage } from "../About/AboutPage";
import { HobbyPage } from "../Hobby/HobbyPage";

const RouterUser = () => {

    const [Data, setData] = useState([]);
    const addData = (newList) => {
        setData(newList);
    };

    return (
        <UserContextProvider>
        <Routes>
            <Route exact path="/" element={<User/>}></Route>
            <Route path="/view/:index" element={<ViewUser />}></Route>
            <Route path="/about" element={<AboutPage />} />
            <Route path="hobby" element={<HobbyPage />} />
        </Routes>
        </UserContextProvider>
    );
}

export default RouterUser;