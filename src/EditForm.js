import React, { useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useState } from "react";

export const EditFormDialog = ({
  nameError,
  addressError,
  hobbyError,
  open,
  setOpen,
  List,
  name,
  address,
  hobby,
  setTempEdit,
  editHandler,
}) => {
  return (
    <div>
      <Dialog open={open}>
        <DialogTitle>Add User</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="name"
            type="text"
            fullWidth
            variant="standard"
            value={name}
            error={nameError && nameError.length?true:false}
            helperText={nameError}
            onChange={(e) => setTempEdit({name:e.target.value})}
          />
          <TextField
            autoFocus
            margin="dense"
            id="address"
            label="address"
            type="text"
            fullWidth
            variant="standard"
            value={address}
            error={addressError && addressError.length?true:false}
            helperText={addressError}
            onChange={(e) => setTempEdit({address:e.target.value})}
          />
          <TextField
            autoFocus
            margin="dense"
            id="hobby"
            label="hobby"
            type="text"
            fullWidth
            variant="standard"
            value={hobby}
            error={hobbyError && hobbyError.length?true:false}
            helperText={hobbyError}
            onChange={(e) =>setTempEdit({hobby:e.target.value})}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
          <Button onClick={() => editHandler()}>save</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
