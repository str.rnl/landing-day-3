import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { BrowserRouter, Route, Router, RouterProvider, Routes, createBrowserRouter } from 'react-router-dom';
import User from './User';
import { UserContextProvider } from './User/userContext';
import RouterUser from './User/router';
import ButtonAppBar from './AppBar';

const router = createBrowserRouter([
  {
    path: "/",
    element: <RouterUser></RouterUser>,
  },{
    path: "/about",
    element: <div>Halaman about</div>,
  },{
    path: "/hobby",
    element: <div>Halaman Hobby</div>,

  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
  <ButtonAppBar></ButtonAppBar>
    <Routes>
      <Route path='/*' element={<RouterUser/>}></Route>
    </Routes>
  </BrowserRouter>
);
