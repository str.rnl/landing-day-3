import React from "react";

function Lists (props) {
    return(
        <div className="List">
            <div className="List-Child">
                <div className="Child-left">
                    <h1 className="Name">{props.name}</h1>
                    <p className="Address">{props.address}</p>
                </div>
                <div className="Child-right">
                    <h1 className="Hobby">{props.hobby}</h1>
                </div>
            </div>
        </div>
    );
}

export default Lists