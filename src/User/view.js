import React from "react";
import { useUserContext } from "./userContext";
import { useParams, useRoutes } from "react-router-dom";

const ViewUser = () => {

    const {index} = useParams()

    const [listUser, setListUser] = useUserContext();

    return(
        <div>
            <div>{listUser[index].name}</div>
            <div>{listUser[index].address}</div>
            <div>{listUser[index].hobby}</div>
        </div>
    );

}

export default ViewUser;