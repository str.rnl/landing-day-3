import React from "react";
import { useUserContext } from "../User/userContext";

export const HobbyPage = () => {

    const [listUser, setListUser] = useUserContext();

  return (
    <div className="hobby">
      <h1>List Hobby saat ini</h1>
      {listUser?.length !== 0 ? (
        listUser?.map((a) => {
          return <div>{a.hobby}</div>;
        })
      ) : (
        <div>0 hobby</div>
      )}
    </div>
  );
};