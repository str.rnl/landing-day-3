import React, { useReducer } from "react";
import { useState } from "react";
import Button from "@mui/material/Button";
import {
  AppBar,
  Box,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
} from "@mui/material";
import { Link, useNavigate, useRoutes } from "react-router-dom";
import { useUserContext } from "./userContext";
import { EditFormDialog } from "../EditForm";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const User = () => {
  const [popups, setPopup] = useState(false);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");
  const [listUser, setListUser] = useUserContext();
  const intialTemp = {
    name:"",
    address:"",
    hobby:"",
    index:-1
  }
  const [tempEdit, setTempEdit] = useReducer((prev, next)=>({...prev, ...next}),
    intialTemp)
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const [nameError, setNameError] = useState("");
  const [addressError, setAddressError] = useState("");
  const [hobbyError, setHobbyError] = useState("");

  const togglePopup = () => {
    setPopup((prev) => {
      if (prev) {
        setName("");
        setAddress("");
        setHobby("");
      }
      return !prev;
    });
  };

  const togglePopupEdit = () => {
    setOpen((prev) => {
      if (prev) {
        setName("");
        setAddress("");
        setHobby("");
      }
      return !prev;
    });
  };

  const addUser = () => {
    setListUser((prev) => [...prev, { name, address, hobby }]);
    togglePopup();
  };

  const inputHandler = (e, names) => {
    if (names === "name") {
      setName(e.target.value);
    }
    if (names === "address") {
      setAddress(e.target.value);
    }
    if (names === "hobby") {
      setHobby(e.target.value);
    }
  };

  const deleteHandler = (name) => {
    setListUser(listUser.filter((a) => a.name !== name));
  };

  const editHandler = (index) => {
    setNameError("")
    setAddressError("")
    setHobbyError("")
    if (!tempEdit.name || !tempEdit.name.length) {
      setNameError("nama harus diisi");
      return false;
    }
    if (!tempEdit.address || !tempEdit.address.length) {
      setAddressError("address harus diisi");
      return false;
    }
    if (!tempEdit.hobby || !tempEdit.hobby.length) {
      setHobbyError("hobby harus diisi");
      return false;
    }
    let tempArr = listUser.map((obj, e) => e == tempEdit.index? ({name: tempEdit.name,
      address: tempEdit.address,
      hobby: tempEdit.hobby}): obj);
    setListUser(tempArr);
    console.log(tempArr);
    setOpen(false);
  };

  const handleClickOpen = (index) => {
    togglePopupEdit()
    setTempEdit({...listUser[index], index})
  };
  return (
    <div className="blank">
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <Button
              className="button"
              onClick={togglePopup}
              variant="contained"
            >
              Add User
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
      <div className="blank-inner">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell align="right">Address</TableCell>
                <TableCell align="right">Hobby</TableCell>
                <TableCell>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listUser.map((row, index) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="right">{row.address}</TableCell>
                  <TableCell align="right">{row.hobby}</TableCell>
                  <TableCell>
                    <Button
                      variant="contained"
                      onClick={() => {
                        navigate(`/view/${index}`);
                      }}
                    >
                      View
                    </Button>
                    <Button
                      variant="contained"
                      onClick={() => {
                        handleClickOpen(index);
                      }}
                    >
                      Edit
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Modal
          open={popups}
          onClose={togglePopup}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <div>
            <Box sx={style}>
              <div className="form">
                <div>
                  <label for="name">Name</label>
                  <input
                    type="text"
                    id="name"
                    value={name}
                    onChange={(e) => {
                      inputHandler(e, "name");
                    }}
                  ></input>
                </div>
                <div>
                  <label for="address">Address</label>
                  <input
                    type="text"
                    id="address"
                    value={address}
                    onChange={(e) => {
                      inputHandler(e, "address");
                    }}
                  ></input>
                </div>
                <div>
                  <label for="hobby">Hobby</label>
                  <input
                    type="text"
                    id="hobby"
                    value={hobby}
                    onChange={(e) => {
                      inputHandler(e, "hobby");
                    }}
                  ></input>
                </div>
                <Button
                  className="close-btn"
                  onClick={addUser}
                  variant="contained"
                >
                  Save
                </Button>
              </div>
            </Box>
          </div>
        </Modal>
        <Modal
        open={open}
        onClose={togglePopupEdit}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description">
          <div>
          <EditFormDialog
              {...{
                nameError,
                addressError,
                hobbyError,
                open,
                setOpen,
                listUser,
                name:tempEdit.name,
                address:tempEdit.address,
                hobby:tempEdit.hobby,
                setTempEdit,
                editHandler,
              }}
            ></EditFormDialog>
          </div>
        </Modal>
      </div>
    </div>
  );
};

export default User;
