import React, { useState } from 'react';

export const UserContext = React.createContext();

export const useUserContext = () => React.useContext(UserContext)

export const UserContextProvider = (props) => {
    const [listUser, setListUser] = useState([]);

    return (<UserContext.Provider value={[listUser, setListUser]}>
        {props.children}
    </UserContext.Provider>);
}